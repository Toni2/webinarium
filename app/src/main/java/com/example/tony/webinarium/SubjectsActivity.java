package com.example.tony.webinarium;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.tony.webinarium.db.DBHelper;

public class SubjectsActivity extends AppCompatActivity {

    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subjects);

        dbHelper = new DBHelper(this);

        SQLiteDatabase db = dbHelper.getWritableDatabase(); // Подключение к базе данных

        Cursor c = db.query("Themes", null, null, null, null,null,null); // таблица с данными

        if (c.moveToFirst()) {

            // определяем номера столбцов по имени в выборке
            int idColIndex = c.getColumnIndex("id");
            int nameColIndex = c.getColumnIndex("name");
            int subjectidColIndex = c.getColumnIndex("subjectid");
            int  infotext = c .getColumnIndex("infotext");

            do {
                // получаем значения по номерам столбцов и пишем все в лог
                Log.d("MYBIGKEK",
                        "ID = " + c.getInt(idColIndex) +
                                ", name = " + c.getString(nameColIndex) +
                                ", subjectsid = " + c.getString(subjectidColIndex) +
                                ", infotext =  " + c.getString(infotext));
                // переход на следующую строку
                // а если следующей нет (текущая - последняя), то false - выходим из цикла
            } while (c.moveToNext());
        }else{

            }

        dbHelper.close();
    }
}
