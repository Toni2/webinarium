package com.example.tony.webinarium.db;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.tony.webinarium.R;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by tony on 19/04/17.
 */

public class DBHelper extends SQLiteOpenHelper {
    private final Context fContext;
    public DBHelper(Context context) {
        // конструктор суперкласса
        super(context, "DataBaseApplication", null, 1);
        fContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("ЛУЛ", "--- onCreate database ---");



        // создаем таблицу с полями
        db.execSQL("create table Subjects ("
                + "id integer primary key autoincrement,"
                + "name text" + ");"); //id integer primary key autoincrement - запрос на создание автоинкремируемого поля

        db.execSQL("create table Themes ("
                + "id integer primary key autoincrement,"
                + "name text,"
                + "subjectid integer," + "infotext text" + ");");

        db.execSQL("create table Test ("
                + "id integer primary key autoincrement," + "question text" + "variantone text," +  "varianttwo text," + "variantthree text,"+ "variantfour text," + "answer text,"
                + "subjectid integer" + "infotext text" + "topicid" + ");");



        // Добавляем записи в таблицу Subjects
        ContentValues valuesSubjects = new ContentValues();
        // Получим массив строк из ресурсов
        Resources resSubjects = fContext.getResources();
        String[] cattable_records = resSubjects.getStringArray(R.array.name_subjects);
        // проходим через массив и вставляем записи в таблицу
        int length = cattable_records.length;
        for (int i = 0; i < length; i++) {
            valuesSubjects.put("name", cattable_records[i]);
            db.insert("Subjects", null, valuesSubjects);
        }


        //Добавляем записи в таблицу Themes

        ContentValues valuesThemes = new ContentValues();

        // Получим файл из ресурсов
        Resources resThemes = fContext.getResources();

        // Открываем xml-файл
        XmlResourceParser xmlThemes = resThemes.getXml(R.xml.themes_records);
        try {
            // Ищем конец документа
            int eventType = xmlThemes.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                // Ищем теги record
                if ((eventType == XmlPullParser.START_TAG)
                        && (xmlThemes.getName().equals("record"))) {
                    // Тег Record найден, теперь получим его атрибуты и
                    // вставляем в таблицу
                    String name = xmlThemes.getAttributeValue(0);
                    String subjectid = xmlThemes.getAttributeValue(1);
                    String infotext = xmlThemes.getAttributeValue(2);
                    valuesThemes.put("name", name);
                    valuesThemes.put("subjectid", subjectid);
                    valuesThemes.put("infotext",infotext);
                    db.insert("Themes", null, valuesThemes);
                }
                eventType = xmlThemes.next();
            }
        }
        // Catch errors
        catch (XmlPullParserException e) {
            Log.e("Test", e.getMessage(), e);
        } catch (IOException e) {
            Log.e("Test", e.getMessage(), e);

        } finally {
            // Close the xml file
            xmlThemes.close();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
